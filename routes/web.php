<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
Route::resource('books', BookController::class);
use App\Http\Controllers\CreaterBookController;
Route::resource('creater_books', CreaterBookController::class);
use App\Http\Controllers\UserController;
Route::resource('users', UserController::class);
Route::post('/', '\App\Http\Controllers\UserController@login');
Route::get('/', '\App\Http\Controllers\UserController@login');
Route::get('/logout', ['as'=>'logout','uses' => '\App\Http\Controllers\BookController@logout']);
Route::post('/reset_password', ['as'=>'logout','uses' => '\App\Http\Controllers\UserController@reset_password']);
Route::get('/reset_password', ['as'=>'logout','uses' => '\App\Http\Controllers\UserController@reset_password']);
Route::get('/hello', '\App\Http\Controllers\UserController@login');


