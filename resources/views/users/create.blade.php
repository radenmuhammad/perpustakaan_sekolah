@extends('template')
 
@section('content')
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create User</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('books.index') }}">Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{route('users.store')}}" method="POST">
    @csrf
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama User:</strong>
                <input type="text" name="name" class="form-control" placeholder="Nama User">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input type="text" name="email" class="form-control" placeholder="Email">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Role Id:</strong>
				<select name="role_id" class="form-control">
					<option value="1">Admin</option>
					<option value="2">User</option>
				</select>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Password:</strong>
                <input type="text" name="password" class="form-control" placeholder="Password">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">No</th>
            <th>User Name</th>
            <th>Role Id</th>
            <th>Action</th>
        </tr>
		@php($count=0)
        @foreach ($users as $user)
		@php($count++)
        <tr>
            <td class="text-center">{{$count}}</td>
            <td>{{ $user->name }}</td>
            <td>
			@if($user->role_id == 1)
				Admin 
			@else
				User
			@endif	
			</td>
            <td class="text-center">
                <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                </form>
            </td>			
        </tr>
        @endforeach
    </table>
@endsection


