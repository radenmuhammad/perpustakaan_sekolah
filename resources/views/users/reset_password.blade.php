@extends('template')
 
@section('content')
@if(empty($message))

@else
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
<form action="" method="POST">
    @csrf
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email</strong>
                <input type="text" name="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
                <strong>New Password</strong>
                <input type="text" name="new_password" class="form-control" placeholder="New Password">
            </div>
        </div>
    </div>
	<input type="submit" name="submit" value="Reset Password" class="form-control"/>
</form>
@endsection