@extends('template')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Edit Book</h2>
            </div>
            <div class="float-right">
                <a class="btn btn-secondary" href="{{ route('books.index') }}"> Back</a>
            </div>
        </div>
    </div>
 
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
 
    <form action="{{ route('books.update',$book->id) }}" method="post">
        @csrf
        @method('PUT')
 
         <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Pengarang Buku:</strong>
				<select name="id_create_book" class="form-control">
					@foreach ($createrbook as $create)
						@if ($create->id == $book->id_create_book)
							<option value="{{$create->id}}" selected>{{$create->create_name}}</option>
						@else
							<option value="{{$create->id}}">{{$create->create_name}}</option>	
						@endif							
					@endforeach					
				</select>
				<script>document.getElementById('id_create_book').value='{{ $book->id_create_book }}';</script>
				<br>				
                    <strong>Buku:</strong>
                    <input type="text" name="book_name" value="{{ $book->book_name }}" class="form-control" placeholder="Book Name">
				<br>				
                    <strong>Informasi Buku:</strong>
                    <input type="text" name="book_information" value="{{ $book->book_information }}" class="form-control" placeholder="Book Information">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </div>
 
    </form>
@endsection