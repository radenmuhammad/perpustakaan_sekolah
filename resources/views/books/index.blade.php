@extends('template')
 
@section('content')
    <div class="row mt-5 mb-5">
        <div class="col-lg-12 margin-tb">
            <div class="float-left">
                <h2>Perpustakaan Sekolah</h2>
            </div>
			@if (Auth::user()->role_id == 1)
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('users.create') }}">Create User</a>
            </div><br><br>				
			@endif	
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('books.create') }}">Create Buku</a>
            </div><br><br>
            <div class="float-right">
                <a class="btn btn-success" href="{{ route('creater_books.create') }}">Create Pencipta Buku</a>
            </div>
        </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">No</th>
            <th>Nama Pencipta</th>
            <th>Nama Buku</th>
            <th>Informasi Buku</th>
            <th width="280px"class="text-center">Action</th>
        </tr>
        @foreach ($books as $book)
        <tr>
            <td class="text-center">{{ ++$i }}</td>
            <td>{{ $book->create_name }}</td>
            <td>{{ $book->book_name }}</td>
            <td>{{ $book->book_information }}</td>
            <td class="text-center">
                <form action="{{ route('books.destroy',$book->id) }}" method="POST">
                    <a class="btn btn-primary btn-sm" href="{{ route('books.edit',$book->id) }}">Edit</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
	<a class="btn btn-success" href="logout">Logout</a>
@endsection