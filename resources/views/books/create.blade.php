@extends('template')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create New Book</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('books.index') }}">Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{ route('books.store') }}" method="POST">
    @csrf
 
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Pengarang Buku:</strong>
				<select name="id_create_book" class="form-control">
					@foreach ($createrbook as $create)
						<option value="{{$create->id}}">{{$create->create_name}}</option>
					@endforeach					
				</select><br>
                <strong>Nama Buku:</strong>
                <input type="text" name="book_name" class="form-control" placeholder="Nama Buku">
                <strong>Informasi Buku:</strong>
                <input type="text" name="book_information" class="form-control" placeholder="Informasi Buku">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
@endsection