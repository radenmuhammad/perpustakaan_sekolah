@extends('template')
 
@section('content')
<div class="row mt-5 mb-5">
    <div class="col-lg-12 margin-tb">
        <div class="float-left">
            <h2>Create Pencipta Buku</h2>
        </div>
        <div class="float-right">
            <a class="btn btn-secondary" href="{{ route('books.index') }}">Back</a>
        </div>
    </div>
</div>
 
@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<form action="{{route('creater_books.store')}}" method="POST">
    @csrf
     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nama Pencipta Buku:</strong>
                <input type="text" name="create_name" class="form-control" placeholder="Nama Pencipta Buku">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
    </div>
</form>
    <table class="table table-bordered">
        <tr>
            <th width="20px" class="text-center">No</th>
            <th>Nama Pencipta Buku</th>
            <th>Action</th>
        </tr>
		@php($count=0)
        @foreach ($createrbooks as $createrbook)
		@php($count++)
        <tr>
            <td class="text-center">{{$count}}</td>
            <td>{{ $createrbook->create_name }}</td>
            <td class="text-center">
                <form action="{{ route('creater_books.destroy',$createrbook->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus data ini?')">Delete</button>
                </form>
            </td>			
        </tr>
        @endforeach
    </table>
@endsection