<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\CreaterBook;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct(){
		if (!!Auth::user()){
			return redirect('/');			
		}		
	}	 
	 
    public function index()
    {
       $books = Book::join('creater_books', 'books.id_create_book', '=', 'creater_books.id')
                ->get(['books.*', 'creater_books.create_name']);
        return view('books.index',compact('books'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }
 
    public function create(){
        $createrbook = CreaterBook::all();		
        return view('books.create',compact('createrbook'));
    }
	 
    public function store(Request $request)
    {
        $request->validate([
            'book_name' => 'required'					
        ]);	
        Book::create($request->all());
        return redirect()->route('books.index')
                        ->with('success','Book created successfully.');
    }
 
    public function show(Book $book)
    {
        return view('books.show',compact('book'));
    }
 
    public function edit(Book $book)
    {
        $createrbook = CreaterBook::all();				
        return view('books.edit',compact('book','createrbook'));
    }
 
    public function update(Request $request, Book $Book)
    {
        $request->validate([
            'book_name' => 'required'
        ]);
 
        $Book->update($request->all());
 
        return redirect()->route('books.index')
                        ->with('success','Book updated successfully');
    }
 
    public function destroy(Book $Book)
    {
        $Book->delete();
        return redirect()->route('books.index')
                        ->with('success','Book deleted successfully');
    }
	
	public function logout(Request $request){
		Auth::logout();
	//	echo "hoho";
		return redirect('/');
	}
}
