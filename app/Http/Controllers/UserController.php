<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index()
    {
		
    }

    public function login(Request $request)
    {
		if ($request->isMethod('post')) {
			$user = User::where('email', $request->username)
					  ->where('password',md5($request->password))
					  ->first();
			if(!empty($user)){
				Auth::login($user);			
				return redirect('/books');		 				
			}else{
				return view('login.index',array("message"=>"Invalid Email Or Password"));						
			}		  
		}
		return view('login.index',array("message"=>""));		
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request){
		//echo Auth::user()->role_id;
		if (Auth::user()->role_id == 1){
			
		}else{
			return redirect('/');			
		}
			
		$users = User::all();
        return view('users.create',compact('users'));		
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		if (Auth::user()->role_id == 1){
			
		}else{
			return redirect('/');			
		}	
        $request->validate([
            'create_name' => 'required',
            'email' => 'required',
            'password' => 'required'					
        ]);		
		$users = $request->all();
		$users['password'] = md5($users['password']);
			$check_email=DB::table('users')
				->where('email', $request->email)
				->first();
			if(!empty($check_email)){
				return redirect()->route('users.create')
                        ->with('success','Email is already exists in the data.');					
			}else{
				User::create($users);				
			}		
        return redirect()->route('users.create')
                        ->with('success','Users created successfully.');		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(User $User)
    {
        $User->delete();
        return redirect()->route('users.create')
                        ->with('success','Users deleted successfully');
    }	
	
	public function reset_password(Request $request){
		if ($request->isMethod('post')) {
			$check_email=DB::table('users')
				->where('email', $request->email)
				->first();
			if(!empty($check_email)){
			DB::table('users')
				->where('email', $request->email)
				->update(['password' => md5($request->password)]);									
				return view('users.reset_password',["message"=>"Reset Password Telah Berhasil"]);					
			}else{
				return view('users.reset_password',["message"=>"Email tidak ditemukan"]);									
			}
		}
        return view('users.reset_password');				
	}
	
}
