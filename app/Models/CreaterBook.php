<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreaterBook extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'create_name'
    ];		
}
